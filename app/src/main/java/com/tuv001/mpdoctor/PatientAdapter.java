package com.tuv001.mpdoctor;


/**
 * Created by JTJ-PC on 27.05.2017.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import java.util.ArrayList;

public class PatientAdapter extends BaseAdapter implements Filterable {
    Context c;
    ArrayList<Patient> patients;

    CustomFilter filter;
    ArrayList<Patient> filterList;

    public PatientAdapter(Context ctx, ArrayList<Patient> patients) {
        // TODO Auto-generated constructor stub
        this.c = ctx;
        this.patients = patients;
        this.filterList = patients;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return patients.size();
    }

    @Override
    public Object getItem(int pos) {
        // TODO Auto-generated method stub
        return patients.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        // TODO Auto-generated method stub
        return patients.indexOf(getItem(pos));
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.listview_patienten, null);
        }
        TextView nachnameTxt = (TextView) convertView.findViewById(R.id.nachname);
        TextView vornameTxt=(TextView)convertView.findViewById(R.id.vorname);
        TextView statusTxt=(TextView)convertView.findViewById(R.id.status);
        //SET DATA TO THEM
        nachnameTxt.setText(patients.get(pos).getNachname());
        vornameTxt.setText(patients.get(pos).getVorname());
        statusTxt.setText(patients.get(pos).getStatus());
        return convertView;
    }

    @Override
    public Filter getFilter() {
        // TODO Auto-generated method stub
        if (filter == null) {
            filter = new CustomFilter();
        }
        return filter;
    }

    //INNER CLASS
    class CustomFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            // TODO Auto-generated method stub
            FilterResults results = new FilterResults();
            if (constraint != null && constraint.length() > 0) {
                //CONSTRAINT TO UPPER
                constraint = constraint.toString().toUpperCase();
                ArrayList<Patient> filters = new ArrayList<Patient>();
                //get specific items
                for (int i = 0; i < filterList.size(); i++) {
                    if (filterList.get(i).getNachname().toUpperCase().contains(constraint)) {
                        Patient p = new Patient(filterList.get(i).getNachname(),filterList.get(i).getVorname(),filterList.get(i).getStatus());
                        filters.add(p);
                    }
                }
                results.count = filters.size();
                results.values = filters;
            } else {
                results.count = filterList.size();
                results.values = filterList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            // TODO Auto-generated method stub
            patients = (ArrayList<Patient>) results.values;
            notifyDataSetChanged();
        }
    }
}
