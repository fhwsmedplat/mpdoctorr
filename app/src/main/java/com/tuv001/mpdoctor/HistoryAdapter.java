package com.tuv001.mpdoctor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;

/**
 * Created by JTJ-PC on 09.06.2017.
 */

public class HistoryAdapter extends BaseAdapter
{
    Context c;
    ArrayList<History> histories;




    public HistoryAdapter(Context ctx, ArrayList<History> histories) {
        // TODO Auto-generated constructor stub
        this.c = ctx;
        this.histories = histories;

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return histories.size();
    }

    @Override
    public Object getItem(int pos) {
        // TODO Auto-generated method stub
        return histories.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        // TODO Auto-generated method stub
        return histories.indexOf(getItem(pos));
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.listview_history, null);
        }
        TextView dateTxt = (TextView) convertView.findViewById(R.id.date);
        TextView diseaseTxt=(TextView)convertView.findViewById(R.id.disease);
        TextView medicineTxt=(TextView)convertView.findViewById(R.id.medicine);
        //SET DATA TO THEM
        dateTxt.setText(histories.get(pos).getDate());
        diseaseTxt.setText(histories.get(pos).getDisease());
        medicineTxt.setText(histories.get(pos).getMedicine());
        return convertView;
    }


}

