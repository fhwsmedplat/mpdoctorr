package com.tuv001.mpdoctor;
//GUI JTJ
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.util.Locale;
//Web service Magommed
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.json.JSONException;
import org.json.JSONObject;






public class ArztLogin extends AppCompatActivity {
    private Button Abutton;//Login
    private EditText username;
    private EditText password;
    private String enteredusernameArzt;
    private String enteredpasswordArzt;
    private Button buttonchangelanguage;//To change the language
    private Locale locale;
    private Configuration config = new Configuration();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arzt_login);

        Abutton = (Button) findViewById(R.id.buttonALogin);
        username = (EditText) findViewById(R.id.etarztname);
        password = (EditText) findViewById(R.id.etarztpassword);
        buttonchangelanguage = (Button) findViewById(R.id.buttonchangelanguage);//buttonchangelanguage correspond with ID buttonchangelanguage from activity_arzt_login.xml

        //Button changelanguage

        buttonchangelanguage.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View view) {
                        showDialog();
                    }});

        //Button Login
        Abutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                enteredpasswordArzt = password.getText().toString();
                enteredusernameArzt = username.getText().toString();

                if (enteredusernameArzt.equals("") || enteredpasswordArzt.equals("")) {
                    Toast.makeText(ArztLogin.this, getString(R.string.messagefilllogin), Toast.LENGTH_LONG).show();
                    //Error msg if username or password is equals " "
                    return;
                }


                //Redirecting to ArtzMain when login is succesful
                //Magommed(Web service) and JTJ(GUI)
                else {
                    RequestParams params = new RequestParams();
                    // Put Http parameter username with value of Email Edit View control
                    params.put("drUsername", enteredusernameArzt);
                    // Put Http parameter password with value of Password Edit Value control
                    params.put("drPassword", enteredpasswordArzt);
                    // Invoke RESTful Web Service with Http parameters
                    AsyncHttpClient client = new AsyncHttpClient();
                    client.get("http://192.168.178.60:8080/MedicineWebservice/Drlogin/doDrlogin", params, new AsyncHttpResponseHandler() {


                        @Override
                        public void onSuccess(String response) {
                            try {
                                // JSON Object
                                JSONObject obj = new JSONObject(response);
                                // When the JSON response has status boolean value assigned with true
                                if (obj.getBoolean("status")) {
                                    //GUI JTJ from *
                                    String language = Locale.getDefault().getLanguage();
                                    Toast.makeText(ArztLogin.this, getString(R.string.welcomedoctor) + " " + enteredusernameArzt, Toast.LENGTH_LONG).show();
                                    Intent toArztmain = new Intent(ArztLogin.this, ArztMain.class);
                                    startActivity(toArztmain);
                                    //to *
                                }
                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                Toast.makeText(ArztLogin.this, getResources().getString(R.string.errorconnection), Toast.LENGTH_LONG).show();//GUI JTJ
                                e.printStackTrace();

                            }
                        }
                    });
                }
            }
        });
    }
        /**
         *Displays a dialog box to choose the new application language
         *When you click on one of the languages, the language of the application is changed
         * And reload the activity to see the changes
         * */
    private void showDialog(){
        AlertDialog.Builder b = new AlertDialog.Builder(this);
        b.setTitle(getResources().getString(R.string.changelanguage));
        // get the array languages of string.xml
        String[] types = getResources().getStringArray(R.array.languages);
        b.setItems(types, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                switch(which){
                    case 0:
                        locale = new Locale("en"); //English (default)
                        config.locale =locale;
                        break;
                    case 1:
                        locale = new Locale("es"); //Spanish
                        config.locale =locale;
                        break;
                    case 2:
                        locale = new Locale("de"); //German
                        config.locale =locale;
                        break;
                }
                getResources().updateConfiguration(config, null);
                Intent refresh = new Intent(ArztLogin.this, ArztLogin.class);
                startActivity(refresh);
                finish();
            }

        });

        b.show();
    }

}

